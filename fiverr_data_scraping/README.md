# Fiverr Data Scraping

This project uses Beautiful Soup to pull data from Fiverr. Then the pandas library to format and output the data in the form of a csv.


## Table of Contents
* [Installation](#Installation)
* [Motivation](#motivation)
* [File Description](#description)
* [Limitations](#Limitations)

## Installation <a name="Installation"></a>
The code requires Python versions of 3.* and general libraries available through the Anaconda package.

## Motivation <a name="motivation"></a>
I wanted to see the difference between Beginner, Level 1 and Level 2 sellers on Fiverr when looking at the products they were providing, the number of reviewers or the review scores.

## File Description <a name="description"></a>
This project will pull data from the Fiverr page specified and output it as a csv. This can be done multiple times as the process is functionised and the csvs can be appended together with the code at the end.

## Limitations <a name="Limitations"></a>

Due to Fiverr security measures you can only scrape the first page each time.
